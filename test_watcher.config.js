module.exports = {
  apply_validation: {
    test_file: 'test/test_apply_validation.js',
    to_test: 'apply_validation.js',
  },
  form_get_routes: {
    test_file: 'test/test_form_get_routes.js',
    to_test: 'form_get_routes.js',
    extra_dependancies: [
      'apply_validation.js',
      'test/form_schemas/form_test_schema.js',
    ],
  },
  post_request_validator: {
    test_file: 'test/test_post_request_validator.js',
    to_test: 'post_request_validator.js',
    extra_dependancies: [
      'test/form_schemas/form_test_schema.js',
      'test/form_schemas/undefined_validation.js',
    ],
  },
  post_to_database: {
    test_file: 'test/test_post_to_database.js',
    to_test: 'post_to_database.js',
    extra_dependancies: [
      'test/form_schemas/form_test_schema.js',
      'test/form_schemas/undefined_validation.js',
    ],
  },
  recipes: {
    test_file: 'test/test_recipes.js',
    to_test: 'recipes.js',
  },
}

