'use strict'
/*eslint-disable no-unused-vars, no-console */

const { bundle_select_form_html } = require('./form_html.js')
// const post_request_validator = require('./post_request_validator.js')
// const post_to_database = require('./post_to_database.js')
const Schema = require('./SchemaObj.js')

const express = require('express')
const multer = require('multer')
const _ = require('lodash')

const fs = require('then-fs')
const path = require('path')


const basic_html_template =
`<!DOCTYPE html>
<html>
<head></head>
<body>{{form_html}}</body>
<script>{{js_bundle}}</script>
</html>
`

/* Bundles the forms to html and saves them in destination_directory */
exports.html2file = async function(schemas_path, destination_directory, {
  html_template = basic_html_template,
}={}) {
  // ensure actual files
  await Promise.all([
    fs.access(schemas_path),
    fs.access(destination_directory),
  ])

  const forms_schemas = await Schema.from_file(schemas_path)

  const forms_keys = forms_schemas
    .query.assignment('module.exports')
    .traverse_to('right')
    .obj.keys()

  const bundler = bundle_select_form_html({html_template, forms_schemas})

  await Promise.all(forms_keys.map(async form_name => {
    await fs.writeFile(
      path.join(destination_directory, `${form_name}.html`),
      await bundler(form_name)
    )
  }))
}


const _wrap_joi = exports._wrap_joi = function(
  joi_validator,
  format_joi_error = ({message}) => message.replace('"value" ', '')
) {
  if (!joi_validator.isJoi) throw new Error('joi_validator must be a joi object')

  return async value => {
    try {
      return await joi_validator.validate(value)
    } catch(err) {
      // https://github.com/hapijs/joi/blob/v13.0.1/API.md#errors
      if (err.name === 'ValidationError')
        throw new Error(err.details.map(format_joi_error).join(', '))

      // if (err.message !== undefined && err.message !== '') throw err

      throw new Error('Unknown Error')
    }
  }
}

const _create_validator = exports._create_validator = function(validate) {
  if (validate.isJoi) return _wrap_joi(validate)

  // promisify regular functions
  if (typeof validate === 'function') return validate

  throw new Error('validate needs to be a Joi validator or a function')
}

const _unexpected_keys = exports._unexpected_keys = function(
  set0,
  set1,
  gen_err_msg = missing_keys => missing_keys.join(', ')
) {
  const diff = _.difference(set0, set1)

  if (diff.length > 0) throw new Error(gen_err_msg(diff.sort()))
}

/* Mutates obj */
const _validate_obj = exports._validate_obj = function(validators) {
  return async obj => {
    let is_error = false
    const results = {}
    const errors = {}
    for (const name in validators) {
      if (validators.hasOwnProperty(name)) {
        try {
          results[name] = await validators[name](obj[name])
        } catch(err) {
          errors[name] = err
          is_error = true
        }
      }
    }
    return is_error ? {errors} : {results}
  }
}

const _is_file_input = exports._is_file_input = function(input_schema) {
  const {attr} = input_schema
  return attr !== undefined &&
         attr.type !== undefined &&
         attr.type.toLowerCase() === 'file'
}

const _get_file_inputs = exports._get_file_inputs = function(input_schemas) {
  return input_schemas.filter(_is_file_input)
}

const _get_reg_inputs = exports._get_reg_inputs = function(input_schemas) {
  return input_schemas.filter(i => !_is_file_input(i))
}

const _validate_obj_middle = exports._validate_obj_middle = function(
  validators,
  property // property on the request object to validate
) {
  this needs to be moved into _validate_route

  const validate = _validate_obj(validators)

  return async function(req, res, next) {
    const {results, errors} = await validate(req[property])

    if (errors === undefined) {
      req[property] = results
      next()
    }
    else next({validation_error: true, errors})
  }
}

const _validate_route = exports._validate_route = function(
  form_schema,
  {
    router_options,
    http_method = 'post',

    text_fields = 'body',

    multer_options = {storage: multer.memoryStorage()},
    multer_fields_keys = ['name', 'maxCount'],
  }
) {
  const middleware = []

  const reg_inputs = _get_reg_inputs(form_schema)
  if (!_.isEmpty(reg_inputs)) {
    middleware.push(_validate_obj_middle(
      _.mapValues(reg_inputs, 'validate'), // validators
      text_fields
    ))
  }

  const file_inputs = _get_file_inputs(form_schema)
  if (!_.isEmpty(file_inputs)) {

    middleware.push(
      multer(multer_options)
        .fields(file_inputs.map(fi => _.pick(fi, multer_fields_keys))
    )
  }

  const route = express.Router(router_options)


}

const validate_middleware = exports.validate_middleware = function(
  schemas,
  {
    base_url = '/',
    url_postfix = '',
    http_method = 'post',
    router_options,
  }
) {
  const router = express.Router(router_options)

  _.each(({validate}, form_name) => {
    router[http_method](
      path.join(base_url, form_name + url_postfix),
      (req, res, next) => {

      }
    )
  })

  return router
}


const form_validate_client = exports.form_validate_client = async function(
  schemas_path,
  on_validated_submit, // function that the page will call when the form is submitted and all inputs are valid
  {
    html_template = basic_html_template,
    base_url = '/',
  }={}
) {
  await fs.access(schemas_path) // valid schemas_path
  const forms_schemas = require(schemas_path),
        forms = Object.keys(forms_schemas)

  if (typeof on_validated_submit === 'function')
    on_validated_submit = `module.exports = ${on_validated_submit.toString()}`
  else if (typeof on_validated_submit !== 'string')
    throw new Error('on_validated_submit must be a function or source string of a function')

  const router = express.Router(),
        bundler = bundle_select_form_html({
          html_template,
          forms_schemas,
          forms_schemas_source: await fs.readFile(schemas_path),
          bundle_form_html_options: {bundle_form_js_options: {on_validated_submit}},
        })

  await Promise.all(forms.map(async form_name => {
    const html = await bundler(form_name)
    router.get(path.join(base_url, `${form_name}.html`), (req, res) => {
      res.type('html')
      res.send(html)
    })
  }))

  return router
}
