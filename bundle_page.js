/*eslint-disable no-console, no-unused-vars */
/*global context, describe, it, __dirname, before, beforeEach, afterEach */

const browserify = require('browserify')
const concat = require('concat-stream-promise')
const str = require('string-to-stream')
const require_stringify = require('require_stringify')

const _ = require('lodash')


/* Returns a browserify promise that will resolve with the bundled js */
exports.bundle_js = bundle_js
function bundle_js(
  entry,  // browserify opts.entries. Reference: https://github.com/browserify/browserify#browserifyfiles--opts
  {
    string_files={}, // module name to module code string. Ex: {foo: 'module.exports = ...', ...}
    basedir, // browserify opts.basedir
  }={}
) {
  return browserify(str(entry), {basedir})
    .plugin(require_stringify)
    .require_stringify(string_files)
    .bundle()
    .pipe(concat())
    .then(bundle => bundle.toString('utf8'))
}


exports.bundle_html = bundle_html
function bundle_html(
  html_template,  // html string that will be filled in
  replace, // object that maps substrings of html_template to be replaced with their string values
) {
  for (const beacon in replace) {
    if (replace.hasOwnProperty(beacon)) {
      if (!html_template.includes(beacon))
        throw new Error(`Could not find beacon "${beacon}" in html_template`)
      html_template = html_template.replace(beacon, replace[beacon])
    }
  }

  return html_template
}
