'use strict'
/*eslint-disable no-console */
/*globals */

const { bundle_js, bundle_html } = require('./bundle_page.js')

const form_builder_default = require('form_builder')

const _ = require('lodash')

const { JSDOM } = require('jsdom')
const { document } = new JSDOM().window
form_builder_default.document = document




/* Returns a function that will generate the html for a specific form key
   Basically to extract the javascript ast forms_schema array values for the bundle_form_html function
*/
exports.bundle_select_form_html = bundle_select_form_html
function bundle_select_form_html({
  html_template, // html template that will be filled in with the form elements and javascript code (review bundle_form_html for default values)
  forms_schemas,
  bundle_form_html_options={}, // options for bundle_form_html function
}) {
  const {obj: form_obj} = forms_schemas.query.assignment_value('module.exports')

  return form_name => bundle_form_html(Object.assign(
    {
      html_template,
      inputs_schema: form_obj.get_properity(form_name).traverse_to('value'),
    },
    bundle_form_html_options,
  ))
}

/* Puts the form html and javascript bundle into the html_template
*/
exports.bundle_form_html = bundle_form_html
async function bundle_form_html({
  html_template, // template that the form html and validate script will be inserted into

  inputs_schema, // Schema object

  // optional
  keep_keys = ['name', 'label', 'attr', 'validate'],
  bundle_form_js_options = {},
  form_html_beacon = '{{form_html}}',
  form_builder = schema => form_builder_default(schema).outerHTML,
  skip_inputs = {form_skip: true},
  js_bundle_beacon = '{{js_bundle}}',
  js_bundle,
}) {

  const array_ast = inputs_schema
    .array.filter(obj_ast => !_.some(skip_inputs, (v, k) => obj_ast.obj.has(k, v)))
    .array.map(obj_ast => obj_ast.obj.filter_keys(keep_keys))

  const filtered_schema = inputs_schema.root
    .replace.assignment('module.exports')
    .right.with(array_ast)

  const html_replace = {}

  html_replace[form_html_beacon] = form_builder(filtered_schema.require())

  html_replace[js_bundle_beacon] = js_bundle ||
    await bundle_form_js(Object.assign(
      {filtered_inputs_code: filtered_schema.source()},
      bundle_form_js_options
    ))

  return bundle_html(html_template, html_replace)
}


exports.bundle_form_js = bundle_form_js
function bundle_form_js({
  filtered_inputs_code, // string module of inputs array 'module.exports = [{...},...]'
  on_validated_submit = 'module.exports = ()=>{}', // string module of function that is called on validated form submit event
  on_invalidated_submit = 'module.exports = ()=>{}', // string module of function that is called on invalidated form submit event
  form_selector = 'form', // css selctor to selecct the form to apply_validation to
  entry, // (optional) browserify entry file
  transform = x=>x,
}) {

  entry = entry || `
    const apply_validation = require('./apply_validation.js')

    const filtered_inputs_code = require('filtered_inputs_code')
    const on_validated_submit = require('on_validated_submit')
    const on_invalidated_submit = require('on_invalidated_submit')

    const form = document.querySelector('${form_selector}')

    apply_validation(
      form,
      filtered_inputs_code,
      {on_validated_submit, on_invalidated_submit}
    )
  `

  const string_files = {
    filtered_inputs_code,
    on_validated_submit,
    on_invalidated_submit,
  }

  const bundle = bundle_js(
    entry,
    {string_files}
  )

  return transform(bundle)
}


// function form_get_routes(htmls, base_url='') {
//   const router = express.Router()

//   for (const base_url in htmls) {
//     if (htmls.hasOwnProperty(base_url)) {
//       router.get('/' + form_type, (req, res) => {
//         res.set('Content-Type', 'text/html')
//         res.send(page_html)
//       })

//     }
//   }

//   return router
// }

// function default_on_validated_submit(form) {
//   const XHR = new XMLHttpRequest()

//   // Define what happens on successful data submission
//   XHR.addEventListener('load', function(event) {
//     alert(event.target.responseText)
//   })

//   // Define what happens in case of error
//   XHR.addEventListener('error', function(event) {
//     alert('Oups! Something goes wrong.')
//   })

//   // Set up our request
//   XHR.open('POST', './' + window.form_type)

//   // The data sent is what the user provided in the form
//   XHR.send(new FormData(form))
// }



