const Joi = require('joi')

module.exports = {
  form0: [
    {
      name: 'input0',
      label: 'Input 0',
      attr: {type: 'text'},
      validate: Joi.string().alphanum().required(),
      database_type: 'string',
    },
  ],
}
