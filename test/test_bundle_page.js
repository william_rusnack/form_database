/*eslint-disable no-console, no-unused-vars */
/*global context, describe, it, __dirname, before, beforeEach, afterEach */

const { bundle_js, bundle_form_js, bundle_html } = require('../bundle_page.js')

const chai = require('chai')
chai.use(require('sinon-chai'))
chai.should()
const { expect } = chai


describe('bundle_js', function() {
  it('simple function evaluates', async function() {
    let spy_value = false
    const spy_func = function() {spy_value = true}

    const entry = `(${spy_func.toString()})()`
    const js_bundle = await bundle_js(entry)

    eval(js_bundle)
    spy_value.should.be.true
  })

  it('require strings', async function() {
    let spy_value = false
    const spy_func = function(a) {spy_value = true}

    const entry = 'require("test_require")()'
    const string_files = {
      test_require: `module.exports = ${spy_func.toString()}`,
    }

    const js_bundle = await bundle_js(entry, {string_files})

    eval(js_bundle)
    spy_value.should.be.true
  })
})

describe('bundle_html', function() {
  it('template missing beacon', function() {
    expect(() => bundle_html('', {hi: ''}) )
      .to.throw('Could not find beacon "hi" in html_template')
  })

  it('replaces correct', function() {
    bundle_html('{{hi}} {{there}}', {
      '{{hi}}': 'hi',
      '{{there}}': 'there',
    })
      .should.equal('hi there')
  })
})

