module.exports = {
  undefined_validation: [
    {
      name: 'input0',
      label: 'Input 0',
      attr: {type: 'text'},
      validate: undefined,  // leave as undefined
      type: 'string',
    },
    {
      name: 'input1',
      label: 'Input 1',
      attr: {type: 'text'},
      validate: undefined,  // leave as undefined
      type: 'string',
    },
  ],
}
