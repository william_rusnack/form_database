'use strict'
/*eslint-disable no-console, no-unused-vars */
/*global describe, it */

const {
  bundle_form_js,
  bundle_form_html,
  bundle_select_form_html,
} = require('../form_html.js')
const Schema = require('../SchemaObj.js')

const form_builder = require('form_builder')

const Tree = require('ast-query')
const acorn = require('acorn')

const jsdom = require('jsdom')
const { JSDOM } = jsdom

const chai = require('chai')
chai.use(require('chai-as-promised'))
chai.use(require('sinon-chai'))
chai.should()
const { expect } = chai

const sinon = require('sinon')

require('mocha-unhandled')


describe('bundle_form_js', async function() {
  it('no errors', async function() {
    const { document } = new JSDOM().window
    form_builder.document = document

    const filtered_inputs_code = '[{name: "input_name"}]'

    const form = form_builder(eval(filtered_inputs_code))
    document.body.appendChild(form)

    const bundle = await bundle_form_js({
      filtered_inputs_code: `module.exports = ${filtered_inputs_code}`,
      on_validated_submit: 'module.exports = ()=>{}',
    })

    eval(bundle)
  })

  it('on_validated_submit called on valid form submit', async function() {
    const { window } = new JSDOM()
    const { document, Event } = window
    form_builder.document = document

    const on_validated_spy = sinon.spy()

    const filtered_inputs_code = `[
      {
        name: "input_name",
        validate: on_validated_spy,
      }
    ]`.replace(/ +/g, ' ')

    const form = form_builder(eval(filtered_inputs_code))
    document.body.appendChild(form)

    eval(await bundle_form_js({
      filtered_inputs_code: `module.exports = ${filtered_inputs_code}`,
      on_validated_submit: 'module.exports = ()=>{}',
    }))
    on_validated_spy.should.not.have.been.called

    form.dispatchEvent(new Event('submit'))

    on_validated_spy.should.have.been.calledOnce
  })

  it('can bundle Joi', async function() {
    this.timeout(5000)

    const filtered_inputs_code = `
      const Joi = require('joi')
      module.exports = [{name: 'joi_input', validate: Joi.any()}]
    `

    const js_bundle = await bundle_form_js({
      filtered_inputs_code,
      on_validated_submit: 'module.exports = () => \'can bundle Joi\'',
    })

    js_bundle.should.be.a('string')
      .and.include('Joi')
  })
})


describe('bundle_form_html', function() {
  // function root_js_node(js_code) {
  //   // returns the root node not the program node
  //   return acorn.parse(js_code).body[0].expression
  // }

  const root_array = create_SchemaType_factory('array')

  const DOCTYPE = '<!DOCTYPE html>'
  const below_script = '<p>below script<p>'
  const html_template = `
    ${DOCTYPE}
    <html>
    <head></head>

    <body>
      {{form_html}}
    </body>

    </html>
    <script>{{js_bundle}}</script>
    ${below_script}
  `.replace(/ +/g, ' ')

  it('inserts js', async function() {
    const schema_array = '[{ name: \'hi\' }]',
          schema_source = `module.exports = ${schema_array}`,
          root_schema = Schema.from_source(schema_source),
          inputs_schema = root_schema.set_ast(root_node_parse(schema_array))

    const js_inserted = 'js string inserted'
    const bundle_form_js_options = {
      on_validated_submit: `module.exports = function(form) {return '${js_inserted}'}`,
    }

    const form_html = '<form>check for this </form>'

    const html_bundle = await bundle_form_html({
      html_template,
      inputs_schema,
      bundle_form_js_options,
      form_builder() {return form_html},
    })

    html_bundle
      .should.be.a('string')
      .and.include(js_inserted)
      .and.include(schema_array)

      .and.include(form_html)

      .and.include(DOCTYPE)
      .and.include(below_script)
  })

  it('inserts form html', async function() {
    const schema_array = '[{ name: \'hi\' }]',
          schema_source = `module.exports = ${schema_array}`,
          root_schema = Schema.from_source(schema_source),
          inputs_schema = root_schema.set_ast(root_node_parse(schema_array))

    const js_bundle = '"js bundle script"'

    const form_builder_options_str = 'not_a_good_http_method'
    function form_builder_wrapper(schema) {
      return form_builder(
        schema,
        {form_attributes: {method: form_builder_options_str}}
      ).outerHTML
    }

    const html_bundle = await bundle_form_html({
      form_builder: form_builder_wrapper,
      html_template,
      inputs_schema,
      js_bundle,
    })

    html_bundle.should.be.a('string')
      .and.include(form_builder_options_str)
      .and.include(`<form method="${form_builder_options_str}">`)
      .and.include('</form>')
      .and.include('<input name="hi">')

      .and.include(js_bundle)

      .and.include(DOCTYPE)
      .and.include(below_script)
  })

  describe('filters inputs schema', function() {
    describe('inputs', function() {
      it('form_skip', async function() {
        const should_be_found = 'should_be_found',
              should_not_be_found = 'should_not_be_found',
              schema_array = `[
                                { name: '${should_be_found}' },
                                { form_skip: true, name: '${should_not_be_found}' },
                              ]`,
              schema_source = `module.exports = ${schema_array}`,
              root_schema = Schema.from_source(schema_source),
              inputs_schema = root_schema.set_ast(root_node_parse(schema_array))

        const html_bundle = await bundle_form_html({
          html_template,
          inputs_schema,
        })

        html_bundle.should.include(should_be_found)
          .and.not.include(should_not_be_found)
          .and.not.include('form_skip')
      })

      it('custom skip', async function() {
        const should_be_found = 'should_be_found',
              should_not_be_found_1 = 'should_not_be_found_1',
              should_not_be_found_2 = 'should_not_be_found_2',
              schema_array = `[
                                { name: '${should_be_found}' },
                                { skip_1: true, name: '${should_not_be_found_1}' },
                                { skip_2: true, name: '${should_not_be_found_2}' },
                              ]`,
              schema_source = `module.exports = ${schema_array}`,
              root_schema = Schema.from_source(schema_source),
              inputs_schema = root_schema.set_ast(root_node_parse(schema_array))

        const html_bundle = await bundle_form_html({
          skip_inputs: {skip_1: true, skip_2: true},
          html_template,
          inputs_schema,
        })

        html_bundle.should.include(should_be_found)
          .and.not.include(should_not_be_found_1)
          .and.not.include(should_not_be_found_2)
      })

    })

    describe('keys', function() {
      it('default', async function() {
        const schema_array = `[{
                                name: 'name-should_be_found',
                                label: 'label-should_be_found',
                                attr: {mode: 'attr-should_be_found'},
                                validate(val) {return 'validate_function_return'},

                                bad_key: 'bad_key-should_not_be_found',
                              }]`,
              schema_source = `module.exports = ${schema_array}`,
              root_schema = Schema.from_source(schema_source),
              inputs_schema = root_schema.set_ast(root_node_parse(schema_array))

        const should_be_found = [
          'name', 'name-should_be_found',
          'label', 'label-should_be_found',
          'attr', 'attr-should_be_found',
          'validate', 'validate_function_return',
        ]
        const should_not_be_found = [
          'bad_key', 'bad_key-should_not_be_found',
        ]

        const html_bundle = await bundle_form_html({
          html_template,
          inputs_schema,
        })

        should_be_found.forEach(str => html_bundle.should.include(str))
        should_not_be_found.forEach(str => html_bundle.should.not.include(str))
      })

      it('custom', async function() {
        const schema_array = `[{
                                name: 'name-should_be_found',
                                rando_key_1: 'rando_key_1-should_be_found',
                                rando_key_2: 'rando_key_2-should_be_found',

                                bad_key: 'bad_key-should_not_be_found',
                                bad_key2: 'bad_key2-should_not_be_found',
                              }]`,
              schema_source = `module.exports = ${schema_array}`,
              root_schema = Schema.from_source(schema_source),
              inputs_schema = root_schema.set_ast(root_node_parse(schema_array))

        const should_be_found = [
          'name', 'name-should_be_found',
          'rando_key_1', 'rando_key_1-should_be_found',
          'rando_key_2', 'rando_key_2-should_be_found',
        ]
        const should_not_be_found = [
          'bad_key', 'bad_key-should_not_be_found',
          'bad_key2', 'bad_key2-should_not_be_found',
        ]

        const html_bundle = await bundle_form_html({
          keep_keys: ['name', 'rando_key_1', 'rando_key_2'],
          html_template,
          inputs_schema,
        })

        should_be_found.forEach(str => html_bundle.should.include(str))
        should_not_be_found.forEach(str => html_bundle.should.not.include(str))
      })
    })
  })


})


describe('bundle_select_form_html', function() {
  describe('argument checks', function() {
    it('form_schemas "module.exports" not found', function() {
      expect(() => bundle_select_form_html({
        html_template: '',
        forms_schemas: Schema.from_source('"No module.exports"'),
      }) )
        .to.throw('Could not find "module.exports" in ast')
        // would prefer the below error message
        // .to.throw('Could not find "module.exports" in forms_schemas_source')
    })

    it('form_schema "module.exports" is not not assigned an Object', function() {
      expect(() => bundle_select_form_html({
        forms_schemas: Schema.from_source('module.exports = "hi"'),
        html_template: '',
      }) )
        .to.throw('Root of ast must be an "ObjectExpression" node but is type "Literal". Line: 1')
        // would prefer the below error message
        // .to.throw('"module.exports" must be directly assigned an object: module.exports = {...}')
    })
  })

  it('two forms', async function() {
    const forms_schemas = Schema.from_source(`
    module.exports = ({
      form0: [{name: 'crazy_name0', validate: ()=>'crazy_validate0'}],
      form1: [{name: 'crazy_name1', validate: ()=>'crazy_validate1'}],
    })`)

    const should_be_found = ['<body>', '<script>']
    const should_be_found_by_form = {
      form0: ['crazy_name0', 'crazy_validate0'],
      form1: ['crazy_name1', 'crazy_validate1'],
    }

    const html_func = bundle_select_form_html({
      forms_schemas,
      html_template: '<body>{{form_html}}</body><script>{{js_bundle}}</script>',
    })

    const forms_schemas_loaded = forms_schemas.require()
    for (const form_name in forms_schemas_loaded) {
      if (forms_schemas_loaded.hasOwnProperty(form_name)) {
        const form_html = await html_func(form_name)

        form_html.should.be.a('string')
          .and.not.include('{{form_html}}')
          .and.not.include('{{js_bundle}}')

        should_be_found
          .concat(should_be_found_by_form[form_name])
          .forEach(to_find => form_html.should.include(to_find) )
      }
    }
  })
})

it('on_validated_submit is wayyyyyy wrong !!!!!!!!!!!!!')





function create_SchemaType_factory(type) {
  return js_code => {
    const ast_program = Schema.from_source(js_code)
    return ast_program.set_ast(root_node(ast_program.ast))[type]
  }
}

function root_node_parse(js_code) {
  return root_node(acorn.parse(js_code))
}

function root_node(ast) {
  // returns the root node not the program node
  const root_node = ast.body[0].expression
  if (root_node === undefined) throw new Error('Could not find root node')
  return root_node
}
