'use strict'
/*eslint-disable no-unused-vars, no-console, no-trailing-spaces */
/*globals describe, before, beforeEach, after, afterEach, it, __dirname */

const {
  html2file,
  form_validate_client,
  validate_middleware,
  _wrap_joi,
  _create_validator,
  _unexpected_keys,
  _is_file_input,
  _get_file_inputs,
  _get_reg_inputs,
  _validate_obj,
  // _validate_obj_middle,
  _validate_route,
} = require('../recipes.js')

const express = require('express')
// const KNEX = require('knex')
const Joi = require('joi')
const _ = require('lodash')

const combineErrors = require('combine-errors')
const while_monitoring = require('while_monitoring')

const sinon = require('sinon')
const supertest = require('supertest')

const jsdom = require('jsdom')
const { JSDOM } = jsdom
const Event = while_monitoring.Event = new JSDOM().window.Event

const chai = require('chai')
chai.use(require('chai-as-promised'))
chai.use(require('sinon-chai'))
chai.should()
const { expect } = chai

const fs = require('then-fs')
const path = require('path')

require('mocha-unhandled')


const writeTestFile = require('./writeTestFile.js')(path.join(__dirname, 'test_recipes_files'))

const destination_directory = path.join(__dirname, 'test_recipes_files/tmp_files')
async function clean_test_dir() {
  return Promise.all(
    (await fs.readdir(destination_directory))
      .map(filename => path.join(destination_directory, filename))
      .map(filepath => fs.unlink(filepath))
  )
}
before(clean_test_dir)
afterEach(clean_test_dir)


describe('html2file', function() {
  const test_schema_str_obj = `({
    form0: [{name: "form_zero"}],
    form1: [{name: "form_one"}],
  })`
  const test_schema_str_module = `module.exports = ${test_schema_str_obj}`
  const test_schema = eval(test_schema_str_obj)

  let schema_path = undefined
  before(async function() {
    schema_path = await writeTestFile(
      'html2file_schema.js',
      test_schema_str_module
    )
  })

  it('error if schema_path does not exist', function() {
    return html2file('bad_schema_path', destination_directory)
      .should.be.rejectedWith('ENOENT: no such file or directory, access \'bad_schema_path\'')
  })

  it('error if destination_directory does not exist', function() {
    return html2file(schema_path, 'bad_destination_directory_path')
      .should.be.rejectedWith('ENOENT: no such file or directory, access \'bad_destination_directory_path\'')
  })

  it('file created for each form schema', async function() {
    await html2file(schema_path, destination_directory)

    ;(await fs.readdir(destination_directory))
      .should.eql(Object.keys(test_schema).map(name => name + '.html'))
  })
})

describe.only('validate support functions', function() {
  const Joi = require('joi')

  describe('_wrap_joi', function() {
    it('throws error if not joi Object', function() {
      expect(() => _wrap_joi(function(){}) )
        .to.throw('joi_validator must be a joi object')
    })

    it('throws correct error message', function() {
      const wrapped = _wrap_joi(Joi.string().alphanum())

      return wrapped('%')
        .should.be.rejectedWith('must only contain alpha-numeric characters')
    })

    it('returns modified value', function() {
      const wrapped = _wrap_joi(Joi.string().uppercase())

      return wrapped('a').should.eventually.equal('A')
    })
  })

  describe('_create_validator', function() {
    it('wrapps joi', function() {
      _create_validator(Joi.any())
        .should.be.a('function')
    })

    it('returns function', function() {
      const func = () => {}
      _create_validator(func)
        .should.equal(func)
    })

    it('throws error with unsupported type', function() {
      expect(() => _create_validator(''))
        .to.throw('validate needs to be a Joi validator or a function')
    })
  })

  describe('_unexpected_keys', function() {
    it('set0 contains more', function() {
      const set0 = [1, 2, 3, 4],
            set1 = [1, 3]
      expect(() => _unexpected_keys(set0, set1) )
        .to.throw('2, 4')
    })

    it('set1 contains more', function() {
      const set0 = [1, 3],
            set1 = [1, 2, 3, 4]
      expect(_unexpected_keys(set0, set1)).to.be.undefined
    })

    it('gen_err_msg outputs correctly', function() {
      const set0 = [1, 2, 3, 4],
            set1 = [1, 3]

      expect(() => _unexpected_keys(set0, set1, keys => `hi ${keys.join(' ')} bye`))
        .to.throw('hi 2 4 bye')
    })
  })

  describe('_is_file_input', function() {
    it('attr is undefined', function() {
      _is_file_input({}).should.be.false
    })

    it('type is undefined', function() {
      _is_file_input({attr: {}}).should.be.false
    })

    it('type is incorrect', function() {
      _is_file_input({attr: {type: 'text'}}).should.be.false
    })

    it('type is correct', function() {
      _is_file_input({attr: {type: 'fIlE'}}).should.be.true
    })
  })

  describe('_get_file_inputs', function() {
    it('contains file input', function() {
      const file_input = {attr: {type: 'file'}}

      _get_file_inputs([
        {},
        file_input,
        {attr: {}},
        file_input,
      ])
        .should.eql([file_input, file_input])
    })
  })

  describe('_get_reg_inputs', function() {
    it('contains file input', function() {
      const file_input = {attr: {type: 'file'}}

      _get_reg_inputs([
        {},
        file_input,
        {attr: {}},
        file_input,
      ])
        .should.eql([{}, {attr: {}}])
    })
  })


  describe('_validate_obj', function() {
    [
      {
        name: 'function invalid',
        validators: {a() {throw new Error('function error')}},
        obj: {a: null},
        errors: {a: 'function error'},
      },
      {
        name: 'function valid',
        validators: {a(val) {return val + 1}},
        obj: {a: 1},
        results: {a: 2},
      },
      {
        name: 'arrow function invalid',
        validators: {a: () => {throw new Error('arrow function error')}},
        obj: {a: null},
        errors: {a: 'arrow function error'},
      },
      {
        name: 'arrow function valid',
        validators: {a: val => val + 2},
        obj: {a: 2},
        results: {a: 4},
      },
      {
        name: 'promise return invalid',
        validators: {a: () => Promise.reject(new Error('promise return error'))},
        obj: {a: null},
        errors: {a: 'promise return error'},
      },
      {
        name: 'promise return valid',
        validators: {a: val => Promise.resolve(val + 1)},
        obj: {a: 2},
        results: {a: 3},
      },
      {
        name: 'async function invalid',
        validators: {a: async () => {throw new Error('async function error')}},
        obj: {a: null},
        errors: {a: 'async function error'},
      },
      {
        name: 'async function valid',
        validators: {a: async val => val + 2},
        obj: {a: 2},
        results: {a: 4},
      },
      {
        name: 'all types valid',
        validators: {
          a(val) {return val + 1},
          b: val => val + 2,
          c: val => Promise.resolve(val + 3),
          d: async val => val + 4,
        },
        obj: {a: 1, b: 2, c: 3, d: 4},
        results: {a: 2, b: 4, c: 6, d: 8},
      },
      {
        name: 'all types valid',
        validators: {
          a() {throw new Error('function error')},
          b: () => {throw new Error('arrow function error')},
          c: () => Promise.reject(new Error('promise return error')),
          d: async () => {throw new Error('async function error')},
        },
        obj: {a: 1, b: 2, c: 3, d: 4},
        errors: {
          a: 'function error',
          b: 'arrow function error',
          c: 'promise return error',
          d: 'async function error',
        },
      },
    ].forEach(({name, validators, obj, results, errors}) => {
      if (results === undefined && errors === undefined)
        throw new Error(`results or errors must be defined for: ${name}`)
      if (results !== undefined && errors !== undefined)
        throw new Error(`results and errors cannot both be defined for: ${name}`)

      it(name, async function() {
        const {
          errors: errors_test,
          results: results_test,
        } = await _validate_obj(validators)(obj)

        expect(results_test).to.eql(results)
        expect(
          errors_test ?
            _.mapValues(errors_test, e => e.message) :
            undefined
        ).to.eql(errors)
      })
    })
  })

  describe('_validate_obj_middle', function() {
    it('invalid', async function() {
      const err_msgs = {a: 'error a', b: 'error b'},
            errors = _.mapValues(err_msgs, msg => new Error(msg)),
            error_validators = _.mapValues(errors, err => () => {throw err}),
            req = {body: _.mapValues(err_msgs, () => null)}

      const next_spy = sinon.spy()

      await _validate_obj_middle(error_validators, 'body')(req, undefined, next_spy)

      next_spy.should.have.been.calledOnce
      next_spy.args[0].should.eql([{validation_error: true, errors}])
    })

    it('valid', async function() {
      const body1 = {a: 1, b: 2},
            validators = {a: x=>x+1, b: x=>x+2},
            ref_results = {a: 2, b: 4},
            req = {body1}

      const next_spy = sinon.spy()

      await _validate_obj_middle(validators, 'body1')(req, undefined, next_spy)

      next_spy.should.have.been.calledOnce
        .and.calledWith() // called with nothing

      req.body1.should.eql(ref_results)
    })
  })
})

describe('validate_middleware', function() {
  it('valid schemas_path path', function() {
    return validate_middleware('bad_schema_path')
      .should.be.rejectedWith('ENOENT: no such file or directory, access \'bad_schema_path\'')
  })

  it('throws error if validate is not defined for every input', async function() {
    const schemas = `
      module.exports = {
        form: [
          {name: "input0", validate: () => {} },
          {name: "input1"},
          {name: "input2", validate: () => {} },
        ]
      }
    `
    const schemas_path = await writeTestFile('validate_middleware-undefined_validate.js', schemas)

    return validate_middleware(schemas_path)
      .should.be.rejectedWith('undefined input validate')
  })

  describe('with router', function() {
    const valid_response = 'valid_response',
          invalid_response = 'val not "a"'

    let router, middleware_req
    before(async function() {

      const schemas = `
        module.exports = {
          validate_request: [{
            name: 'input0',
            validate(val) {
              if (val === 'a') return 'b'
              throw new Error('${invalid_response}')
            },
          }]
        }
      `
      const schemas_path = await writeTestFile(
        'validate_middleware-with_router.js',
        schemas
      )

      const app = express()
      app.use((req, res, next) => {middleware_req = req})
      app.use(await validate_middleware(schemas_path))
      app.use((req, res, next) => {middleware_req = req})
      app.use((req, res) => {res.send(valid_response)})

      router = supertest(app)
    })
    afterEach(() => {middleware_req = undefined})

    it('sends invalid response for invalid data', function() {
      return router.post('./validate_request')
        .field('input0', 'not a')
        .expect(400)
        .expect(invalid_response)
    })

    it('when all valid passes onto next handler', function() {

    })
  })
})


describe.skip('form_validate_client', function() {
  const test_schema_str_obj = `({
    form0: [{name: "form_zero"}],
    form1: [{name: "form_one"}],
    with_validate: [{
      name: "input0",
      validate: val => {if (val === 'a') return 'b'; else throw new Error('must be "a"');},
    }],
  })`
  const test_schema_str_module = `module.exports = ${test_schema_str_obj}`
  const test_schema = eval(test_schema_str_obj)

  let schema_path = undefined
  before(async function() {
    schema_path = await writeTestFile(
      'form_validate_client_schema.js',
      test_schema_str_module
    )
  })

  it('error if schema_path does not exist', function() {
    return form_validate_client('bad_schema_path')
      .should.be.rejectedWith('ENOENT: no such file or directory, access \'bad_schema_path\'')
  })

  it('creates get routes', async function() {
    const base_url = '/test/base/url'
    const app = express()
    app.use(await form_validate_client(
      schema_path,
      () => 'test function',
      {base_url}
    ))
    const router = supertest(app)

    for (const form_name in test_schema) {
      if (test_schema.hasOwnProperty(form_name)) {
        await router.get(path.join(base_url, `${form_name}.html`))
          .expect('Content-Type', /html/)
          .expect(200)
      }
    }
  })


  const virtualConsole = new jsdom.VirtualConsole()
  const vc_errors = []
  virtualConsole.on('log', console.log.bind(console)) //eslint-disable-line no-console
  virtualConsole.sendTo({error(_, err) {vc_errors.push(err)}})
  const check_vc_error = check_for_factory(vc_errors, 'VirtualConsole errors')
  afterEach(check_vc_error)

  async function load_response_page({text: page_html}) {
    if (typeof page_html !== 'string') throw new Error(`page_html must be a string but is: ${page_html}`)
    page_html += `
      <script>
        if (window.run_if_loaded_for_test !== undefined)
          window.run_if_loaded_for_test()
        else window.test_page_is_loaded = true
      </script>
    `

    const dom = new JSDOM(page_html, {
      runScripts: 'dangerously',
      resources: 'usable',
      virtualConsole,
    })
    const { window } = dom

    await new Promise((resolve, reject) => {
      if (window.test_page_is_loaded === true) resolve()
      else {
        window.run_if_loaded_for_test = resolve

        const timeout_error = new Error('Page load timed out')
        setTimeout(() => reject(timeout_error), 200)
      }
    })

    check_vc_error()
    return dom
  }


  ;[ //eslint-disable-line no-extra-semi
    {type: 'string', on_validated_submit: 'module.exports = () => \'test function for "no page error on load"\''},
    {type: 'function', on_validated_submit: () => 'test function for "no page error on load"'},
  ].forEach(({type, on_validated_submit}) => {

    let router = undefined
    before(async () => {
      const app = express()
      app.use(await form_validate_client(
        schema_path,
        on_validated_submit,
      ))
      router = supertest(app)
    })

    it(`no page error on load with ${type}`, async function() {
      for (const form_name in test_schema) {
        if (test_schema.hasOwnProperty(form_name)) {
          const dom = await load_response_page(
            await router.get(`/${form_name}.html`).expect(200)
          )
        }
      }
    })

    it('page validates input', async function() {
      const { window: { document } } = await load_response_page(
        await router.get('/with_validate.html').expect(200)
      )

      const input = document.querySelector('input[name=input0]')
      input.value = 'a'
      await while_monitoring(input)
        .expect('valid')
        .upon_event('blur')

      input.value.should.equal('b')
    })

    it('page invalidates input', async function() {
      const { window: { document } } = await load_response_page(
        await router.get('/with_validate.html').expect(200)
      )

      const input = document.querySelector('input[name=input0]')
      input.value = 'not correct'
      await while_monitoring(input)
        .expect('invalid')
        .upon_event('blur')

      input.value.should.equal('not correct')
    })
  })

  it('page calls on_validated_submit', async function() {
    const app = express()
    app.use(await form_validate_client(
      schema_path,
      form => window.on_validated_submit_test(form),
    ))
    const router = supertest(app)

    const { window } = await load_response_page(
      await router.get('/with_validate.html').expect(200)
    )
    const { document } = window

    const ovs_spy = sinon.spy()
    window.on_validated_submit_test = ovs_spy

    const input = document.querySelector('input[name=input0]')
    input.value = 'a'

    const form = document.querySelector('form')
    form.dispatchEvent(new Event('submit'))
    await release_thread()

    ovs_spy.should.have.been.calledOnce
      .and.calledWith(form)
  })
})


// describe('form2validate2database', function() {

//   let knex_db, db_destroyed = true
//   async function new_database() {
//     if (db_destroyed) {
//       knex_db = KNEX({
//         client: 'sqlite3',
//         connection: {filename: ':memory:'},
//         useNullAsDefault: true,
//       })

//       db_destroyed = false
//     }
//   }
//   async function destroy_database() {
//     await knex_db.destroy()
//     db_destroyed = true
//   }

//   beforeEach(new_database)
//   afterEach(destroy_database)


// })


function release_thread(timeout=0) {
  return new Promise(resolve => setTimeout(resolve, timeout))
}

function check_for_factory(error_array, base_error_msg) {
  return function() {
    if (error_array.length > 0) {
      error_array.unshift(new Error(base_error_msg))
      const combined_errors = combineErrors(error_array)
      error_array.length = 0
      throw combined_errors
    }
  }
}

const unhandled_requests = []
const check_for_unhandled_requests = check_for_factory(
  unhandled_requests,
  'Recived uncaught requests',
)
afterEach(check_for_unhandled_requests)

const route_errors = []
const check_for_route_errors = check_for_factory(
  route_errors,
  'Route errors'
)
afterEach(check_for_route_errors)




// describe.skip('run app', async function() {

//   const schema_path = path.join(__dirname, 'form_schemas/form_test_schema.js')

//   function check_for_factory(error_array, base_error_msg) {
//     return function() {
//       if (error_array.length > 0) {
//         error_array.unshift(new Error(base_error_msg))
//         const combined_errors = combineErrors(error_array)
//         error_array.length = 0
//         throw combined_errors
//       }
//     }
//   }

//   const unhandled_requests = []
//   const check_for_unhandled_requests = check_for_factory(
//     unhandled_requests,
//     'Recived uncaught requests',
//   )
//   afterEach(check_for_unhandled_requests)

//   const route_errors = []
//   const check_for_route_errors = check_for_factory(
//     route_errors,
//     'Route errors'
//   )
//   afterEach(check_for_route_errors)

//   const run_if_loaded_for_test = 'run_if_loaded_for_test',
//         test_page_is_loaded = 'test_page_is_loaded'

//   async function load_app(schema_name) {
//     const app = express()

//     app.use(bodyParser.json())
//     app.use(bodyParser.urlencoded({ extended: false }))

//     const template_basic = `
//       <!DOCTYPE html>
//       <html>
//       <head></head>
//       <body>
//       {{form_html}}
//       </body>
//       </html>

//       <script>var form_type = {{form_type}}</script>
//     `

//     try {
//       app.use('/', await form_get_routes({
//         template: template_basic,
//         schema_path: resolve_path(schema_name),
//         entry_callback() {
//           if (window[run_if_loaded_for_test] !== undefined)
//             window[run_if_loaded_for_test]()
//           else window[test_page_is_loaded] = true
//         },
//       }))
//     } catch(err) {
//       throw combineErrors([new Error('Problem creating app'), err])
//     }

//     check_for_unhandled_requests()
//     app.use(function(req, res) {
//       unhandled_requests.push(
//         new Error(req.method + ':' + req.originalUrl)
//       )
//       res.status(500)
//       res.send('was not caught by routes')
//     })

//     check_for_route_errors()
//     app.use(function(err, req, res) {
//       route_errors.push(err)
//       res.status(err.status || 500)
//       res.send('error')
//     })

//     return supertest(app) // request
//   }

//   const vc_errors = []
//   const check_vc_error = check_for_factory(
//     vc_errors,
//     'VirtualConsole errors'
//   )
//   afterEach(check_vc_error)

//   async function load_page(form_type, request) {
//     if (typeof request === 'string')
//       request = await load_app(request)

//     let page_html = (await request.get('/' + form_type)).text

//     page_html += `
//       <script>
//         if (window.run_if_loaded_for_test !== undefined)
//           window.${run_if_loaded_for_test}()
//         else
//           window.${test_page_is_loaded} = true
//       </script>
//     `

//     const virtualConsole = new jsdom.VirtualConsole()
//     virtualConsole.on('log', console.log.bind(console)) //eslint-disable-line no-console

//     virtualConsole.sendTo({
//       error(_, err) {vc_errors.push(err)},
//     })

//     const dom = new JSDOM(
//       page_html,
//       {
//         runScripts: 'dangerously',
//         virtualConsole,
//       }
//     )
//     const { window } = dom

//     // wait for page load
//     await new Promise((resolve, reject) => {
//       if (window[test_page_is_loaded] === true) resolve()
//       else {
//         window[run_if_loaded_for_test] = resolve

//         // check for VirtualConsole errors and hanging page loads
//         const timeout_error = new Error('Page load timed out')
//         setTimeout(() => {
//           try {
//             check_vc_error()
//             reject(timeout_error)
//           } catch(err) {
//             reject(err)
//           }
//         }, 200)
//       }
//     })

//     check_vc_error()

//     return {
//       dom,
//       window,
//       document: window.window.document,
//       Event: window.Event,
//     }
//   }

//   it('Page should load without error.', async function() {
//     const form_type = 'form0'
//     const { window } = await load_page(form_type, 'schema_simple.js')

//     expect(window.form_type).to.equal(form_type)
//     expect(window.apply_validation).to.be.a('function')
//   })

//   it('post request should be made on form submit', async function() {
//     const form_type = 'form1'
//     const { window, document, Event } = await load_page(form_type)
//     const form = document.querySelector('form')

//     all_good(document, form_type)

//     const XMLHttpRequest = new MockXhrFactory()
//     window.XMLHttpRequest = XMLHttpRequest
//     window.eval('XMLHttpRequest = window.XMLHttpRequest')

//     const xhr = await new Promise((resolve, reject) => {
//       XMLHttpRequest.onSend = resolve
//       form.dispatchEvent(new Event('submit'))
//       setTimeout(() => reject(new Error('timed out')), 50)
//     })

//     expect( xhr.method ).to.equalIgnoreCase('post')

//     expect( xhr.body ).to.be.a('FormData')

//     expect( _.fromPairs(Array.from(xhr.body.entries())) )
//       .to.eql({
//         input0: '1',
//         input1: 'good input1',
//         input2: 'GOOD INPUT2',
//       })
//   })

//   describe('page form_schema should not contain unneeded data', function() {

//   })
// })

