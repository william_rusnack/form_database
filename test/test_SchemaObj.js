'use strict'
/*eslint-disable no-console */
/*global __dirname, describe, it */

const Schema = require('../SchemaObj.js')

const acorn = require('acorn')

const chai = require('chai')
chai.use(require('chai-shallow-deep-equal'))
const { expect } = chai
chai.should()

require('mocha-unhandled')

const { join: pjoin } = require('path')
const just_clone = require('just-clone')

const writeTestFile = require('./writeTestFile.js')(pjoin(__dirname, 'test_SchemaObj_files'))



const src_obj_str = '({hi: "there"})',
      source = `module.exports = ${src_obj_str}`


describe('loads scheama from', async function() {
  const ref_ast = acorn.parse(source)

  it('file', async function() {
    const filepath = await writeTestFile('loads-file-path', source),
          schema_obj = await Schema.from_file(filepath)

    schema_obj.filepath.should.equal(filepath)
    schema_obj.origional_source.should.equal(source)

    schema_obj._query.should.be.an('object')
    schema_obj.ast.should.shallowDeepEqual(ref_ast)

    // should be frozen
    expect(() => schema_obj.hi = null)
      .to.throw('Cannot add property hi, object is not extensible')
  })

  ;[
    {
      test_name: 'string class',
      base: Schema,
    },
    {
      test_name: 'string instance',
      base: new Schema(),
    },
  ].forEach(({test_name, base}) => {
    it(test_name, async function() {
      const schema_obj = base.from_source(source)

      schema_obj.origional_source.should.equal(source)
      schema_obj.root.should.eql(schema_obj)

      schema_obj._query.should.be.an('object')
      schema_obj.ast.should.shallowDeepEqual(ref_ast)

      // should be frozen
      expect(() => schema_obj.hi = null)
        .to.throw('Cannot add property hi, object is not extensible')
    })
  })
})

describe('_copy', function() {
  it('returns new obj', function() {
    const original = new Schema()
    const _copy = original._copy()

    original.should.not.equal(_copy)
  })

  it('is not frozen', function() {
    const returned = (new Schema())._copy()
    returned.hi = 1
    returned.hi.should.equal(1)
  })
})

describe('copy', function() {
  it('is frozen', function() {
    const returned = (new Schema()).copy()
    expect(() => returned.hi = null)
      .to.throw('Cannot add property hi, object is not extensible')
  })
})

describe('traverse_to', function() {
  it('will not traverse_to undefined', function() {
    const program = Schema.from_source('""')
    const bad_key = 'bad key'

    expect(() => program.traverse_to(bad_key))
      .to.throw(`Cannot traverse_to "${bad_key}`)
  })

  it('correct', function() {
    const program = Schema.from_source('""')

    program.traverse_to('body')
      .should.eql(program.set_ast(program.ast.body))
  })
})

describe('set_ast', function() {
  it('sets_correct', function() {
    const returned = (new Schema()).set_ast(5)

    returned.ast.should.equal(5)
  })

  it('is copy', function() {
    const original = new Schema()
    const returned = original.set_ast({})
    original.should.not.equal(returned)
  })

  it('is frozen', function() {
    const returned = (new Schema()).set_ast({})
    expect(() => returned.hi = null)
      .to.throw('Cannot add property hi, object is not extensible')
  })

  it('origional_source & root remains the same', function() {
    const original = Schema.from_source('var a = 1')
    const returned = original.set_ast({})

    original.origional_source.should.eql(returned.origional_source)
    original.should.eql(returned.root)
  })
})

describe('query', function() {

  const source = Schema.from_source('module.exports = "hi"')
  const query_obj = source.query //eslint-disable-line prefer-destructuring

  it('assignment', function() {
    const result = query_obj.assignment('module.exports')

    result.should.be.an.instanceof(Schema)
    result.ast.should.be.an('object')
      .and.have.property('type', 'AssignmentExpression')
  })

  it('cannot find assignment', function() {
    expect(() => query_obj.assignment('not.found'))
      .to.throw('Could not find "not.found" in ast')
  })

  it('assignment_value', function() {
    const result = query_obj.assignment_value('module.exports')

    result.should.be.an.instanceof(Schema)
    result.ast.should.be.an('object')
      .and.have.property('type', 'Literal')
  })

  it('queries current ast not original', function() {
    const sub = source.set_ast(source.ast.body[0].expression.right)
    expect(() => sub.query.assignment('module.exports'))
      .to.throw('Could not find "module.exports" in ast')
  })
})

describe('line_number', function() {
  it('sub ast', function() {
    const program = Schema.from_source('\n\n\nmodule.exports = 5\n')
    const {ast: {body: [sub_ast]}} = program
    program.set_ast(sub_ast).line_number()
      .should.equal(4)
  })

  it('program', function() {
    Schema.from_source('\n\n\nmodule.exports = 5\n').line_number()
      .should.equal(1)
  })
})

describe('assert_type_is', function() {
  const source = '"hi"'
  it('throws error for incorrect type', function() {
    expect(() => Schema.from_source(source).assert_type_is('bad type'))
      .to.throw('Root of ast must be an "bad type" node but is type "Program". Line: 1')
  })

  it('returns this if correct type', function() {
    Schema.from_source(source).assert_type_is('Program')
      .should.be.instanceof(Schema)
  })
})

it('source', function() {
  const original_code = 'const x = 5;'
  const final_code = 'var a = 1;'

  Schema
    .from_source(original_code)
    .set_ast(acorn.parse(final_code))
    .source()
    .should.eql(final_code)
})

it('require', function() {
  const obj_str = '({hi: "there"})',
        obj = eval(obj_str),
        module_str = `module.exports = ${obj_str}`

  Schema
    .from_source(module_str)
    .require()
    .should.eql(obj)
})

describe('ObjectExpression root ast', function() {
  const create_SchemaObject = create_SchemaType_factory('obj')

  describe('get_properity', function() {
    it('returns property', function() {
      const js_code = `({
        a: 1,
        b: 2,
        c: 3,
      })`
      const ref = eval(js_code)

      const ast_obj = create_SchemaObject(js_code)

      for (const key in ref) {
        if (ref.hasOwnProperty(key)) {
          const prop = ast_obj.get_properity(key)
          prop.should.be.instanceof(Schema)
          prop.ast.key.name.should.equal(key)
          prop.ast.value.value.should.equal(ref[key])
        }
      }
    })

    it('is not an ObjectExpression node, throws error', function() {
      const js_code = '\n\n\n[]'

      expect(() => create_SchemaObject(js_code) )
        .to.throw('Root of ast must be an "ObjectExpression" node but is type "ArrayExpression". Line: 4')
    })

    it('throws error if key is not found', function() {
      const js_code = '({})'
      const ast_obj = create_SchemaObject(js_code)

      expect(() => ast_obj.get_properity('hi') )
        .to.throw('Cannot find key "hi"')
    })
  })

  describe('has', function() {
    it('has key but not value', function() {
      const key = 'key_str', value = 'value_str',
            js_code = `( {${key}: 'not_value_str'} )`

      create_SchemaObject(js_code)
        .has(key, value)
        .should.be.false
    })

    it('has value but not key', function() {
      const key = 'key_str', value = 'value_str',
            js_code = `( {not_key_str: '${value}'} )`

      create_SchemaObject(js_code)
        .has(key, value)
        .should.be.false
    })

    it('is not an ObjectExpression node, throws error', function() {
      const js_code = '\n\n\n[]'

      expect(() => create_SchemaObject(js_code) )
        .to.throw('Root of ast must be an "ObjectExpression" node but is type "ArrayExpression". Line: 4')
    })

    it('has key and value', function() {
      const key = 'key_str',
            value = 'value_str',
            js_code = `( {${key}: '${value}'} )`

      create_SchemaObject(js_code)
        .has(key, value)
        .should.be.true
    })

    it('keys', function() {
      const js_code = '({a: 1, b: 2, z: 3})'

      create_SchemaObject(js_code).keys()
        .should.eql(['a', 'b', 'z'])
    })
  })

  describe('filter_keys', function() {
    it('filters keys', function() {
      const js_code = `(
        {
          remain1: 'remain1 val',
          remove: 'remove val',
          remain2: 'remain2 val',
        }
      )`
      const ref_node = root_node_from_source(js_code)
      ref_node.properties.splice(1, 1)

      create_SchemaObject(js_code)
        .filter_keys(['remain1', 'remain2'])
        .ast.should.shallowDeepEqual(ref_node)
    })

    it('does not modify the origional Node', function() {
      const js_code = `(
        {
          remove: 'remove val',
        }
      )`
      const obj_node = root_node_from_source(js_code),
            obj_node_copy = root_node_from_source(js_code)

      create_SchemaObject(js_code).filter_keys([])
        .should.not.eql(obj_node)

      obj_node.should.eql(obj_node_copy)
    })

    it('throws if not given an ObjectExpression Node', function() {
      const js_code = '\n\n"not an Object"\n'

      expect(() => create_SchemaObject(js_code).filter_keys([]) )
        .to.throw('Root of ast must be an "ObjectExpression" node but is type "Literal". Line: 3')
    })
  })
})

describe('ArrayExpression root ast', function() {
  const create_SchemaArray = create_SchemaType_factory('array')

  function root_array(js_code) {
    const program_schema = Schema.from_source(js_code),
          array_schema = program_schema.set_ast(program_schema.ast.body[0].expression)

    if (array_schema.ast.type !== 'ArrayExpression') throw new Error('Must be an ArrayExpression')

    return array_schema
  }

  it('throws error if not ArrayExpression', function() {
    expect(() => create_SchemaArray('"HI"'))
      .to.throw('Root of ast must be an "ArrayExpression" node but is type "Literal". Line: 1')
  })

  it('filter', function() {
    const js_code = '[1,2,3,4]',
          program_schema = Schema.from_source(js_code),
          array_schema = program_schema.set_ast(program_schema.ast.body[0].expression)

    const ref_ast = just_clone(array_schema.ast)
    ref_ast.elements = ref_ast.elements.slice(0, 2)
    const ref_schema = array_schema.set_ast(ref_ast)

    create_SchemaArray(js_code).filter(x => x.ast.value <= 2)
      .should.be.instanceof(Schema)
      .and.eql(ref_schema)
  })

  it('map', function() {
    const js_code = '[1,2]'

    const ref_schema = Object.assign(
      new Schema(),
      Schema.from_source(js_code),
      {
        ast: root_array('[2,4]').ast,
        _query: undefined,
      },
    )

    const original = create_SchemaArray(js_code)

    const mapped = original.map(w => {
      const new_ast = Object.assign({}, w.ast)
      new_ast.value = w.ast.value * 2
      new_ast.raw = String(w.ast.value * 2)
      return w.set_ast(new_ast)
    })

    Object.assign(
      new Schema(),
      mapped,
      {_query: undefined},
    )
      .should.eql(ref_schema)
  })
})


describe('SchemaReplace', function() {
  describe('assignment', function() {
    it('replace assignment right', function() {
      const js_code = `
        const a = 1;
        module.exports = 2;
        const b = 3;
      `
      const ref0 = Schema.from_source(js_code),
            ref1 = Schema.from_source(js_code)

      const filler = 'fill in value'
      const replacement = new Schema()
      replacement.ast = filler

      const modified = ref0
        .replace.assignment('module.exports')
        .right.with(replacement)

      ref0.should.eql(ref1)
      modified.should.not.eql(ref0)

      modified.query.assignment('module.exports').ast.right
        .should.equal(filler)
    })

    it('cannot select "right" before "assignment"', function() {
      const schema = new Schema()
      expect(() => schema.replace.right )
        .to.throw('Cannot select property "right" before selecting this.subtree with "assignment(query)"')
    })

    it('"right" is not defined in "subtree"', function() {
      const schema_replace = (new Schema()).replace
      schema_replace.subtree = {}

      expect(() => schema_replace.right )
        .to.throw('this.subtree.right is not defined')
    })
  })

  describe('with', function() {
    describe('cannot be called before the following is defined', function() {
      it('subtree', function() {
        const schema = new Schema()
        expect(() => schema.replace.with())
          .to.throw('"this.subtree" must be defined before "with" is called')
      })

      it('property', function() {
        const schema_replace = (new Schema()).replace
        schema_replace.subtree = 'not undefined'

        expect(() => schema_replace.with())
          .to.throw('"this.property" must be defined before "with" is called')
      })
    })

    it('argument must be a Schema instance', function() {
      const schema_replace = (new Schema()).replace
      schema_replace.subtree = 'not undefined'
      schema_replace.property = 'not undefined'

      expect(() => schema_replace.with('not a schema instance'))
        .to.throw('replacement must be a Schema instance')
    })
  })
})


function root_node(ast) {
  // returns the root node not the program node
  const root_node = ast.body[0].expression
  if (root_node === undefined) throw new Error('Could not find root node')
  return root_node
}

function root_node_from_source(js_code) {
  // returns the root node not the program node
  return root_node(acorn.parse(js_code))
}

function create_SchemaType_factory(type) {
  return js_code => {
    const ast_program = Schema.from_source(js_code)
    return ast_program.set_ast(root_node(ast_program.ast))[type]
  }
}



// describe('filter_array_and_replace_elements', function() {
//   it('array_node must be an ArrayExpression', function() {
//     const js_code = '\n"not an ArrayExpression"'
//     const node = root_js_node(js_code)

//     expect( () => load_ast(js_code).filter_array_and_replace_elements(node) )
//       .to.throw('array_node must be an ArrayExpression node. Line: 2')
//   })

//   it('filters out elements', function() {
//     const js_code = '[1,2,3]'

//     const array_node = root_js_node(js_code)
//     const ref_node = root_js_node(js_code)
//     ref_node.elements.splice(1, 1)

//     const { filter_array_and_replace_elements } = load_ast(js_code)
//     filter_array_and_replace_elements(array_node, {
//       filter_func(node) {return node.value !== 2},
//     })
//       .should.eql(ref_node)
//   })

//   it('replaces array elements', function() {
//     const js_code = '[1,2,3]'

//     const array_node = root_js_node(js_code)

//     const ref_node = root_js_node(js_code)
//     ref_node.elements = eval(js_code).map(x => 2 * x)

//     const { filter_array_and_replace_elements } = load_ast(js_code)
//     filter_array_and_replace_elements(array_node, {
//       replace_func(node) {return 2 * node.value},
//     })
//       .should.eql(ref_node)
//   })

//   describe('does not modify the origional Node', function() {
//     [
//       {
//         test_name: 'filter_func',
//         options: {filter_func(node) {node.value < 2}},
//       },
//       {
//         test_name: 'replace_func',
//         options: {replace_func(node) {2 * node.value}},
//       },
//     ].forEach(({test_name, options}) => {
//       it(test_name, function() {
//         const js_code = '[1,2,3,4]'

//         const array_node = root_js_node(js_code),
//               array_node_copy = root_js_node(js_code)

//         const { filter_array_and_replace_elements } = load_ast(js_code)
//         filter_array_and_replace_elements(array_node, options)
//           .should.not.eql(array_node)

//         array_node.should.eql(array_node_copy)
//       })
//     })
//   })
// })

