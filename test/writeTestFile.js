'use strict'
/*globals after */

const { isAbsolute, join: pjoin } = require('path')
const { unlink, writeFile } = require('then-fs')

module.exports = function(base_directory) {
  const test_files = []
  after(() => Promise.all(test_files.map(file => unlink(file))))

  return async function writeTestFile(filepath, data) {
    filepath = isAbsolute(filepath) ? filepath :
      pjoin(base_directory, filepath)

    if (test_files.includes(filepath))
      throw new Error(`Test file has already been written: ${filepath}`)

    if (data === undefined)
      throw new Error('writeTestFile data must not be undefined')

    test_files.push(filepath)
    await writeFile(filepath, data)

    return filepath
  }
}
