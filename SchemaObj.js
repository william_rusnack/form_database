'use strict'
/*eslint-disable no-console */

const escodegen = require('escodegen')
const Tree = require('ast-query')
const requireFromString = require('require-from-string')
const just_clone = require('just-clone')

const fs = require('then-fs')

// Reference: https://github.com/BebeSparkelSparkel/AST-query#program
const ast_query_methods = [
  'assignment',
]


const Schema = module.exports = function Schema() {
  this.filepath = undefined // file path of souce (if loaded from string: undefined)

  this.root = undefined // root Schema that this instance was derived from
  this.origional_source = undefined // string of orgional souce file

  this._query = undefined // ast-query

  this.ast = undefined // subtree defaults to assigned value of module.exports
}

Schema.from_file  = async function(filepath) {
  const inst = new Schema()

  inst.filepath = filepath

  return inst.from_source((await fs.readFile(filepath)).toString())
}

Schema.from_source = Schema.prototype.from_source = function(source_str) {
  const inst = this instanceof Schema ? this : new Schema()

  inst._query = Tree.fromSource(source_str)

  inst.root = inst
  inst.origional_source = source_str

  inst.ast = inst._query.tree

  return Object.freeze(inst)
}

Schema.prototype.copy = function() {
  return Object.freeze(this._copy())
}

Schema.prototype._copy = function() {
  return Object.assign(new Schema(), this)
}

Schema.prototype.traverse_to = function(property) {
  const dest = this.ast[property]

  if (dest === undefined) throw new Error(`Cannot traverse_to "${property}`)

  return this.set_ast(dest)
}

/* sets the ast of a copy to ast */
Schema.prototype.set_ast = function(ast) {
  const inst = this._copy()
  inst.ast = ast
  inst._query = Tree.fromTree(ast)

  return Object.freeze(inst)
}

/* gives line number of this.ast.start */
Schema.prototype.line_number = function() {
  return (this.origional_source.slice(0, this.ast.start).match(/\n/g) || []).length + 1
}

/* throws error if this.ast.type is not type */
Schema.prototype.assert_type_is = function(type) {
  if (this.ast.type !== type)
    throw new Error(`Root of ast must be an "${type}" node but is type "${this.ast.type}". Line: ${this.line_number()}`)

  return this
}

/* takes the ast and returns the generaged code as a string */
Schema.prototype.source = function(options) {
  return escodegen.generate(this.ast, options)
}

/* interprets the current ast and acts as a regular require */
Schema.prototype.require = function() {
  return requireFromString(this.source())
}

Object.defineProperty(Schema.prototype, 'query', {
  get() {return new SchemaQuery(this)},
})

Object.defineProperty(Schema.prototype, 'obj', {
  get() {return new SchemaObject(this)},
})

Object.defineProperty(Schema.prototype, 'array', {
  get() {return new SchemaArray(this)},
})

Object.defineProperty(Schema.prototype, 'replace', {
  get() {return new SchemaReplace(this)},
})


function SchemaQuery(schema) {
  this._schema = schema
}

ast_query_methods.forEach(method => {
  SchemaQuery.prototype[method] = function(query) {
    const {nodes: [ query_result ]} = this._schema._query[method](query)

    if (query_result === undefined)
      throw new Error(`Could not find "${query}" in ast`)

    return this._schema.set_ast(query_result)
  }

  SchemaQuery.prototype[`${method}_value`] = function(query) {
    const {ast: {right}} = this[method](query)
    return this._schema.set_ast(right)
  }
})


function SchemaObject(schema) {
  this._schema = schema.assert_type_is('ObjectExpression')
}

SchemaObject.prototype.get_properity = function(key) {
  return this._schema.set_ast(this._get_properity(key))
}

SchemaObject.prototype.has = function(key, value) {
  try {
    return this._get_properity(key).value.value === value
  } catch(err) {
    return false
  }
}

SchemaObject.prototype.keys = function() {
  const { properties } = this._schema.ast
  return properties.map(p => p.key.name)
}

SchemaObject.prototype._get_properity = function(key) {
  const { properties } = this._schema.ast
  for (let pi = 0; pi < properties.length; ++pi) {
    if (properties[pi].key.name === key)
      return properties[pi]
  }

  throw new Error(`Cannot find key "${key}"`)
}

SchemaObject.prototype.filter_keys = function(keep_keys) {
  /* removes the keys from an Object that are not in the Array keep_keys */

  const copy = Object.assign({}, this._schema.ast)

  copy.properties = this._schema.ast.properties
    .filter(({key: {name}}) => keep_keys.includes(name))

  return this._schema.set_ast(copy)
}


function SchemaArray(schema) {
  this._schema = schema.assert_type_is('ArrayExpression')
  this._copy = Object.assign({}, this._schema.ast)
}

SchemaArray.prototype.filter = function(func) {
  this._copy.elements = this._schema.ast.elements.filter(
    node => func(this._schema.set_ast(node))
  )
  return this._schema.set_ast(this._copy)
}

SchemaArray.prototype.map = function(func) {
  this._copy.elements = this._schema.ast.elements.map(
    node => func(this._schema.set_ast(node)).ast
  )
  return this._schema.set_ast(this._copy)
}


/* Example command:
    schema.replace.assignment('module.exports').right.with(schema2)
*/
function SchemaReplace(schema) {
  this.schema = schema

  this.subtree = undefined
  this.property = undefined

  this.replacer = undefined
}

SchemaReplace.prototype.copy = function() {
  return Object.assign(new SchemaReplace(), this)
}

SchemaReplace.prototype.define = function(property, value) {
  const inst = this.copy()
  inst[property] = value
  return inst
}

// ast_query_methods.forEach
SchemaReplace.prototype.assignment = function(query) {
  const to_return = this.define(
    'subtree',
    this.schema.query.assignment(query).ast
  )
  return to_return
}

;[
  'right',
].forEach(property => {
  Object.defineProperty(SchemaReplace.prototype, property, {
    get() {
      // if assignment gets generalized this error message will have to be updated
      if (this.subtree === undefined) throw new Error(`Cannot select property "${property}" before selecting this.subtree with "assignment(query)"`)
      if (this.subtree[property] === undefined) throw new Error(`this.subtree.${property} is not defined`)

      return this.define('property', property)
    },
  })
})

SchemaReplace.prototype.with = function(replacement) {
  if (this.subtree === undefined) throw new Error('"this.subtree" must be defined before "with" is called')
  if (this.property === undefined) throw new Error('"this.property" must be defined before "with" is called')
  if (!(replacement instanceof Schema)) throw new Error('replacement must be a Schema instance')

  return this.schema.set_ast(replace_subtree(
    this.schema.ast,
    this.subtree,
    this.property,
    replacement.ast
  ))
}

/* Returns a new instance of main_tree with the parent[property] replaced with replacer
   main_tree is not modified
   main_tree - a tree that contains the sub-tree to be replaced
   parent - parent of the sub-tree to be replaced (should be in main tree)
   property - property of parent that should be replaced
   replacer - tree that will be inserted into the new main tree
*/
function replace_subtree(main_tree, parent, property, replacer) {
  const archive = parent[property]
  parent[property] = replacer
  const to_return = just_clone(main_tree)
  parent[property] = archive
  return to_return
}
